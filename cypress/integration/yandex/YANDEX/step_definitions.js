import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps';
import { YandexPage } from '../pageObject/YandexPage';

let yandexPage = new YandexPage();

When('on the ya page I focus on text input', () => {
  yandexPage.getSearchField().click();
});

When('on the ya page I type {string}', (text) => {
  yandexPage.getSearchField().type(text);
});

When('on the ya page I click button {string}', (value) => {
  yandexPage.getSearchButton().click();
  // cy.xpath(`//input[@value="${value}"]`).first().click();
});

Then('on the ya page appear search result', () => {
  yandexPage.getSearchResult().should('be.visible');
});

Then('on the ya page i see url {string}', function(url) {
  yandexPage.checkUrl(url);
});
Then(/^on the ya page search field contains "([^"]*)"$/, function(text) {
  yandexPage.getSearchField().should('have.value', text);
});

When(/^курсор установлен на поле ввода поискового запроса$/, function() {
  yandexPage.getSearchField().click();
});

And(/^я ввёл следующий текст "([^"]*)"$/, function(text) {
  yandexPage.getSearchField().type(text);
});

And(/^проверил, что поле ввода содержит слово "([^"]*)"$/, function(text) {
  yandexPage.getSearchField().should('have.value', text);
});

And(/^я выполнил клик по кнопке "([^"]*)"$/, function() {
  yandexPage.getSearchButton().click();
});

Then(/^каждый элемент поисковой выдачи содержит искомый текст "([^"]*)"$/, function(text) {
  yandexPage.getSearchResult().each(($el, index) => {
    if (index !== 0)
      cy.wrap($el).should(el => {
        let el_text = el.text().toLowerCase()
        expect(el_text).to.contains(text.toLowerCase())
      });
  });
});