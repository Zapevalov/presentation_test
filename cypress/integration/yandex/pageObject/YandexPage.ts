
export class YandexPage{

  getSearchField(){
    return cy.get("input.input__control.input__input.mini-suggest__input")
  }

  getSearchButton(){
    return cy.contains('Найти')
  }

  getSearchResult(){
    return cy.get("ul[aria-label='Результаты поиска'] > li")
  }

  checkUrl(url: String) {
    return cy.url()
      .should('be.equal', url)
  }
}

